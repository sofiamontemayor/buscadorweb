/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package directorio;

/* Imports */
// import java.awt.Desktop;
import java.io.IOException;
import java.io.File;
import java.io.BufferedWriter;
import java.io.FileWriter;

/**
 *
 * @author MLG_2
 */
public class Directorio {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        leerArchivos();
    }
    
    /**
     * Barre la carpeta de archivos y crea el archivo log.
     */
    public static void leerArchivos() {
        double tiempoInicio = System.currentTimeMillis();
        String rutaFiles = "src/Files";
        File folder = new File(rutaFiles);
        File[] listOfFiles = folder.listFiles();
        String contenido = "";
        double tiempoTotal = 0;
        for (File listOfFile : listOfFiles) {
            if (listOfFile.isFile()) {
                // System.out.println("open_file " + listOfFile.getName());
                double tiempoInicioAbrir = System.currentTimeMillis();
                try {
                    // Desktop.getDesktop().open(listOfFile);
                    FileWriter fw = new FileWriter(listOfFile);
                    BufferedWriter bw = new BufferedWriter(fw);
                    bw.close();
                }catch (IOException ex) {

                    System.out.println(ex);

                }
                double tiempoFinalAbrir = System.currentTimeMillis() - tiempoInicioAbrir;
                contenido += listOfFile.getAbsolutePath() + "\t" + (tiempoFinalAbrir / 1000) + "\n";
                tiempoTotal += tiempoFinalAbrir;
            } else if (listOfFile.isDirectory()) {
                // System.out.println("Directory " + listOfFile.getName());
            }
        }
        contenido += "\n \n";
        contenido += "Tiempo total en abrir en archivos: " + (tiempoTotal / 1000) + "\n";
        double tiempoFinal = System.currentTimeMillis() - tiempoInicio;
        contenido += "Tiempo total de ejecución: " + (tiempoFinal / 1000) + "\n";
        crearArchivo(contenido);
    }
    
    /**
     * 
     * Crear Archivo log
     * @param contenido 
     */
    public static void crearArchivo(String contenido){
        try {
            String ruta = "src/log/log_archivos.txt";
            // String contenido = "Contenido de ejemplo";
            File file = new File(ruta);
            // Si el archivo no existe es creado
            if (!file.exists()) {
                file.createNewFile();
            }
            FileWriter fw = new FileWriter(file);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(contenido);
            bw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    
    
}
